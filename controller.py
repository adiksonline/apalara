from core import World

__author__ = 'oreke'
import cmd


class CmdManager(cmd.Cmd):
    def __init__(self):
        cmd.Cmd.__init__(self)
        self.world = World()

    def do_display(self, args):
        print "{}{}".format(" " * (2 + 2*self.world.currentIndex),
                            self.world.currentItem if self.world.currentItem else "-")
        table = self.world.table
        mx = 0
        for i in table:
            if len(i) > mx:
                mx = len(i)
        top = mx - 1
        print "\n" * (5 - top)
        for i in xrange(top, -1, -1):
            print "|",
            for j in table:
                if len(j) > i:
                    print j[i],
                else:
                    print " ",
            print "|"
        print "|" + "_" * 13 + "|"


    def emptyline(self):
        self.world.nop()

    def precmd(self, line):
        if not line.startswith("why"):
            self.world.clearError()
        return line

    def onecmd(self, line):
        try:
            return cmd.Cmd.onecmd(self, line)
        except Exception as e:
            print e

    ############### STATE METHODS ###################

    def do_box(self, args):
        print self.world.box(args.upper())

    def do_on(self, args):
        lst = args.upper().split()
        print self.world.on(*lst)

    def do_clear(self, args):
        print self.world.clear(args.upper())

    def do_ontable(self, args):
        print self.world.ontable(args.upper())

    def do_grasping(self, args):
        print self.world.grasping(args.upper())

    def do_onright(self, args):
        lst = args.upper().split()
        print self.world.onright(*lst)

    def do_onleft(self, args):
        lst = args.upper().split()
        print self.world.onleft(*lst)

    def do_under(self, args):
        lst = args.upper().split()
        print self.world.under(*lst)

    ############### ACTION METHODS ##################

    def do_grasp(self, args):
        self.world.grasp(args.upper())

    def do_whereis(self, args):
        self.world.whereis(args.upper())

    def do_nop(self, args):
        pass

    def do_moveto(self, args):
        self.world.moveto(args.upper())

    def do_free(self, args):
        self.world.free()

    def do_neighbours(self, args):
        self.world.neighbour(args.upper())

    def do_placeontable(self, args):
        self.world.placeontable()

    def do_placeon(self, args):
        self.world.placeon(args.upper())

    def do_placeontop(self, args):
        lst = args.upper().split()
        self.world.placeontop(*lst)

    def do_placeunder(self, args):
        lst = args.upper().split()
        self.world.placeunder(*lst)

    def do_why(self, args):
        self.world.why()

    @staticmethod
    def do_quit(args):
        print "Thank you for choosing Apalara"
        return True

    def do_exit(self, args):
        return self.do_quit(args)

    def do_swap(self, args):
        lst = args.upper().split()
        self.world.swap(*lst)

    def do_stack(self, args):
        lst = args.upper().split()
        self.world.stack(*lst)

    def do_generatestack(self, args):
        n = int(args.split()[0])
        self.world.generatestack(n)

    ################ HELP METHODS ####################

    @staticmethod
    def help_box():
        print "box X: checks if X is a box"

    @staticmethod
    def help_clear():
        print "clear X: checks if there is nothing on X"

    @staticmethod
    def help_display():
        print "display: draws the current state of the world on screen"

    @staticmethod
    def help_exit():
        print "exit: quits the console"

    @staticmethod
    def help_free():
        print "free: drops whatever box is in the robot's claw"

    @staticmethod
    def help_generatestack():
        print "generatestack N: generates a N stack configuration of boxes. Possible values of N are 1, 2, 3 and 6"

    @staticmethod
    def help_grasp():
        print "grasp: grasps the topmost box at the current location of the robot arm"
        print "grasp X: moves to X's location and grasp"

    @staticmethod
    def help_grasping():
        print "grasping: checks if the robot is currently grasping any box"
        print "grasping X: checks if robot is currently grasping box X"

    @staticmethod
    def help_neighbours():
        print "neighbours X: shows a list of box X's neighbours, if any"

    @staticmethod
    def help_nop():
        print "nop: does nothing"

    @staticmethod
    def help_on():
        print "on X Y: checks if box X is directly on Y"

    @staticmethod
    def help_onleft():
        print "onleft X Y: checks if box X is directly on the left of Y"

    @staticmethod
    def help_onright():
        print "onright X Y: checks if box X is directly on the right of Y"

    @staticmethod
    def help_ontable():
        print "ontable X: checks if box X is on the table"

    @staticmethod
    def help_placeon():
        print "placeon X: place the currently grasped box on X"

    @staticmethod
    def help_placeontable():
        print "placeontable: place te currently grasped box on any free table spot"

    @staticmethod
    def help_placeontop():
        print "placeontop X Y: place X directly on top of Y"

    @staticmethod
    def help_placeunder():
        print "placeunder X Y: place X directly under Y"

    def help_quit(self):
        self.help_exit()

    @staticmethod
    def help_stack():
        print "stack A B C... X: stacks all boxes A, B, C... in the current position of X"

    @staticmethod
    def help_under():
        print "under X Y: checks if box X is directly under Y"

    @staticmethod
    def help_whereis():
        print "whereis X: provides information (exact and relative) about the current location of box X"

    @staticmethod
    def help_why():
        print "why: provides information about the reason for a failed operation"

    @staticmethod
    def help_moveto():
        print "moveto X: moves the robot arm to the current location of box X"

    @staticmethod
    def help_swap():
        print "swap X Y: swaps the position of X with Y"


def main():
    cmdManager = CmdManager()
    cmdManager.prompt = ">>> "
    cmdManager.cmdloop("Welcome to Apalara console. Type 'help' for a list of commands to get started")