__author__ = 'oreke'

HOLDING = (-1, -1)


class World:

    def __init__(self):
        self.table = (["A"],["B"],["C"],["D"],["E"], ["F"])
        self.currentItem = None
        self.currentIndex = 0
        self.error = None

    def box(self, obj):
        return obj in ("A", "B", "C", "D", "E", "F")

    def search(self, x):
        if self.currentItem == x:
            return HOLDING
        if self.box(x):
            for i, item in enumerate(self.table):
                if item.count(x):
                    index = item.index(x)
                    break
            return (i, index)

    def on(self, x, y):
        a, b = self.search(y)
        if self.search(x) == (a, b+1):
            return True
        else:
            return False

    def clear(self, x):
        a, b = self.search(x)
        if not self.grasping(x) and len(self.table[a]) == b+1:
            return True
        else:
            return False

    def ontable(self, x):
        a, b = self.search(x)
        return b==0

    def onleft(self, x, y):
        a, b = self.search(y)
        return self.search(x) == (a-1, b)

    def onright(self, x, y):
        a, b = self.search(y)
        return self.search(x) == (a+1, b)

    def grasping(self, x=None):
        if not x:
            return self.currentItem is not None
        return self.search(x) == HOLDING

    def under(self, x, y):
        a, b = self.search(y)
        return self.search(x) == (a, b-1)


    def moveto(self, x):
        if self.clear(x):
            a, b = self.search(x)
            self.currentIndex = a
        else:
            print "Cannot move to {}".format(x)
            self.error = "Box {} is not clear".format(x)

    def free(self):
        a = self.currentIndex
        if self.currentItem:
            self.table[a].append(self.currentItem)
            self.currentItem = None

    def grasp(self, x=None):
        if x:
            if self.currentItem == x:
                print "Will not perform operation"
                self.error = "Box {} is already in robot's claw".format(x)
                return
            elif not self.clear(x):
                print "Cannot grasp box {}".format(x)
                self.error = "Box {} is not clear".format(x)
                return
            else:
                self.moveto(x)
        if self.currentItem is not None:
            print "Cannot grasp box"
            self.error = "Robot is currently holding a box"
        elif not self.table[self.currentIndex]:
            print "This is not possible"
            self.error = "There is nothing to grasp at this location"
        else:
            x = len(self.table[self.currentIndex])  - 1
            self.currentItem = self.table[self.currentIndex][x]
            self.table[self.currentIndex].pop()

    def nop(self):
        pass


    def why(self):
        if self.error:
            print self.error

    def clearError(self):
        self.error = None

    def neighbour(self, x):
        if self.grasping(x):
            print "Box {} has decided not to have neighbours at this time".format(x)
            self.error = "Lol. {} is actually in the robot's claw. #dasall :D".format(x)
            return
        a, b = self.search(x)
        neighbours = []
        table = self.table
        if table[a][-1] != x:
            neighbours.append((table[a][b+1], "is on top of {}"))
        if b > 0:
            neighbours.append((table[a][b-1], "is under {}"))
        if a > 0 and len(table[a-1]) > b:
            neighbours.append((table[a-1][b], "is on left of {}"))
        if a+1 < len(table) and len(table[a+1]) > b:
            neighbours.append((table[a+1][b], "is on right of {}"))
        if neighbours:
            for box, location in neighbours:
                print "{}:".format(box), location.format(x)
        else:
            print "Box {} has no neighbours".format(x)

    def freeLocation(self, *args):
        for i, item in enumerate(self.table):
            if not item or item[-1] not in args:
                return i
        return -1
        pass

    def freeTable(self):
        for i, item in enumerate(self.table):
            if not item:
                return i
        return -1

    def movetoindex(self,x):
        self.currentIndex = x

    def placeontable(self):
        x = self.freeTable()
        if x > -1:
            print "\nOP: MOVETOINDEX {}".format(x); self.movetoindex(x)
            print "OP: FREE\n"; self.free()
            return x
        else:
            print "Cannot place on table"
            self.error = "All points on table are occupied"

    def placeon(self, x):
        if self.clear(x):
            print "\nOP: MOVETO {}".format(x); self.moveto(x)
            print "OP: FREE\n"; self.free()
        else:
            print "Cannot place on {}".format(x)
            self.error = "{} is not clear".format(x)

    def whereis(self, x):
        a, b = self.search(x)
        if self.grasping(x):
            print "{} is in the robot's claw".format(x)
        else:
            print "{} is at coordinates {}".format(x, (a,b))
            if self.table[a][-1] != x:
                print "\tUnder {}".format(self.table[a][b+1])
            if b > 0:
                print "\tOn top of {}".format(self.table[a][b-1])

    def swap(self, x, y):
        print "\nOP: FREE"; self.free()
        if self.clear(x) and self.clear(y):
            a, b = self.search(x)
            c, d = self.search(y)
            index = self.freeLocation(x, y)
            print "OP: GRASP {}".format(x); self.grasp(x)
            print "OP: MOVETOINDEX {}".format(index); self.movetoindex(index)
            print "OP: FREE"; self.free()
            print "OP: GRASP {}".format(y); self.grasp(y)
            print "OP: MOVETOINDEX {}".format(a); self.movetoindex(a)
            print "OP: FREE"; self.free()
            print "OP: GRASP {}".format(x); self.grasp(x)
            print "OP: MOVETOINDEX {}".format(c); self.movetoindex(c)
            print "OP: FREE\n"; self.free()
        else:
            print "Cannot perform swap"
            self.error = "Either of box {} or {} is not clear".format(x, y)

    def placeontop(self, x, y):
        print "\nOP: FREE"; self.free()
        if self.clear(x) and self.clear(y):
            print "OP: GRASP {}".format(x); self.grasp(x)
            self.placeon(y)
        else:
            print "Cannot place box {} on {}".format(x, y)
            self.error = "Either of box {} or {} is not clear".format(x, y)

    def placeunder(self, x, y):
        print "\nOP: FREE"; self.free()
        if self.clear(x) and self.clear(y):
            a, b = self.search(y)
            index = self.freeLocation(x, y)
            print "OP: GRASP {}".format(y); self.grasp(y)
            print "OP: MOVETOINDEX {}".format(index); self.movetoindex(index)
            print "OP: FREE"; self.free()
            print "OP: GRASP {}".format(x); self.grasp(x)
            print "OP: MOVETOINDEX {}".format(a); self.movetoindex(a)
            print "OP: FREE"; self.free()
            print "OP: GRASP {}".format(y); self.grasp(y)
            print "OP: MOVETOINDEX {}".format(a); self.movetoindex(a)
            print "OP: FREE\n"; self.free()
        else:
            print "Cannot place box {} on {}".format(x, y)
            self.error = "Either of box {} or {} is not clear".format(x, y)

    def stack(self, *args):
        x = args[-1]
        a, b = self.search(x)
        for box in args[:-1]:
            if self.search(box)[0] != a:
                if not self.clear(box):
                    print "Cannot continue. Operation aborted"
                    self.error = "Box {} is not clear".format(box)
                    return
                top = self.table[a][-1]
                self.placeontop(box, top)

    def generatestack(self, n):
        if n  in (1, 2, 3, 6):
            sizeh = 6/n
            maxc = n
            holders = filter(lambda x: True if x else False, self.table)
            holders = list(holders[:sizeh])
            while len(holders) < sizeh:
                h = reduce(lambda x, y: x if len(x) > maxc else y, holders)
                print "OP: GRASP {}".format(h[-1]); self.grasp(h[-1])
                holders.append(self.table[self.placeontable()])
            i = 0
            for item in self.table:
                while item:
                    if item in holders:
                        if len(item) <= maxc: break
                        else: i = (i+1) % sizeh if item == holders[i] else i
                    top = holders[i][-1]
                    box = item[-1]
                    self.placeontop(box, top)
                    if len(holders[i]) == maxc:
                        i = (i+1) % sizeh
        else:
            print "This is not possible"
            self.error = "It is not possible to generate a {}-sized stack of boxes".format(n)
